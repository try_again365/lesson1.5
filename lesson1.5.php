<?php
$link = 'http://api.openweathermap.org/data/2.5/weather';
$apiKey = 'd3811b58d69fee25d0ab5928037e69f2';
$city = 'Warszawa';
$units = 'metric';
$apiURL = "{$link}?q={$city}&units={$units}&appid={$apiKey}";

//$buffer = @file_get_contents('http://api.openweathermap.org/data/2.5/weather?id=6695624&APPID=d3811b58d69fee25d0ab5928037e69f2');

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Document</title>
    </head>
	<body>
  <?php

$buffer = @file_get_contents($apiURL);
if ($buffer===false) {
  exit('Не удалось получить данные');
}


$data = json_decode($buffer, true);
if ($data===null) {
  exit('Ошибка декодирования json');
}

function checkData($data) {
  if (empty($data)) {
    return 'не удалось получить данные';}
    return $data;
  }

$temperature = checkData($data['main']['temp']);
$pressure = checkData($data['main']['pressure']);
$humidity = checkData($data['main']['humidity']);
$wind_speed = checkData($data['wind']['speed']);
$sunrise = checkData($data['sys']['sunrise']);
$sunset = checkData($data['sys']['sunset']);

echo $city . '</br>';
echo 'Температура воздуха ' . $temperature . '</br>';
echo 'Давление ' . $pressure . '</br>';
echo 'Влажность ' . $humidity . '</br>';
echo 'Скорость ветра ' . $wind_speed . ' м/с' . '</br>';
echo 'Восход ' . date("d.m.Y h.i.s A", $sunrise) . '</br>';
echo 'Заход ' . date("d.m.Y h.i.s A", $sunset) . '</br>';

?>
	</body>
</html>